<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Suggestion;

class SuggestionController extends Controller
{
	public $suggestion;
	public function __construct()
	{
		$this->suggestion = new Suggestion;
	}
	/**
    * @return view
    */
    public function queryview()
    {
    	return view('query');
    }

    /**
    * @param request data from form 
    */
    public function queryinput(Request $request)
    {
    	$request = $request->validate([
    		'query' => 'required|max:1000'
    	]);

    	$this->suggestion->executequery($request['query']);
    }
}
