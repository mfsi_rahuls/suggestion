<!DOCTYPE html>
<html>
<head>
  <title>Suggestion</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
  <div class="container">
    <form class="col-6 offset-2 mt-4" method="post" action="/query">
      @csrf
      <div class="form-group">
        <label for="query">Query</label>
        <textarea class="form-control" name="query" id="query" rows="3"></textarea>
      </div>
      <button class="btn btn-primary">Run</button>
    </form>
  </div>
</body>
</html>


